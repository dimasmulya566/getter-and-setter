package dimastrimulyasaputro;

class Data{
    private int intPrivate;
    public int intPublic;
    private  double doublePrivate;

    public Data(){
        this.intPrivate = 100;
        this.intPublic = 0;
    }

    void display(){
        System.out.println(this.intPublic);
        System.out.println(this.intPrivate);
        System.out.println(this.doublePrivate);
    }

    // getter
    public int getIntPrivate(){
        return this.intPrivate;
    }

    //setter

    public void setDoublePrivate(double value){
        this.doublePrivate = value;
    }
}

class Lingkaran{
    private double diameter;
    Lingkaran(double diameter){
        this.diameter = diameter;
    }

    //setter
    public void setJari2(double jari2){
        this.diameter = jari2*2;
    }

    //getter
    public double getJari2(){
        return this.diameter/2;

    }

    //getter
    public double getLuas(){
        return 3.34*diameter*diameter/4;
    }
}


public class Main {

    public static void main(String[] args) {
	// write your code here
        Data object = new Data();
        // read and write menggunakan public
        object.intPublic = 20; // write
        System.out.println("Public = " + object.intPublic); // read

        // read only (bisa menggunakan getter)
        int angka = object.getIntPrivate();
        System.out.println("Getter = " + angka);

        // write only (bisa menggunakan setter)
        object.setDoublePrivate(0.39);
        object.display();

        // read and write only dengan menggunakan setter dan getter
        Lingkaran object2 = new Lingkaran(5);
        System.out.println("Jari-jari = " + object2.getJari2());
        object2.setJari2(14);
        System.out.println("Jari-jari = " + object2.getJari2());
        System.out.println("Luas = " + object2.getLuas());

    }
}
